# -*- coding: utf-8 -*-
"""
Created on Fri May 29 10:01:43 2020

@author: user
"""


import geopandas as gpd

fp = "D:/py/gis/Data/pop15/Vaestotietoruudukko_2015.shp"
pop = gpd.read_file(fp)
pop.head()
pop = pop.rename(columns={'ASUKKAITA' : 'pop15'})
pop.columns
pop.tail(2)

selected_cols = ['pop15', 'geometry']
pop = pop[selected_cols]

addr_fp = r"D:/py/gis/Data/addresses_epsg3879.shp"
addresses = gpd.read_file(addr_fp)
addresses.head(2)
addresses.crs
pop.crs
addresses.crs == pop.crs

join = gpd.sjoin(addresses, pop, how="inner", op="within")
join.head()

outfp = r"D:/py/gis/Data/Data/Result/addresses_pop15_epsg3979.shp"
join.to_file(outfp)


import matplotlib.pyplot as plt
join.plot(column="pop15", cmap="Reds", markersize=7, scheme='natural_breaks', legend=True);
plt.title("Amount of inhabitants living close the the point");
plt.tight_layout()