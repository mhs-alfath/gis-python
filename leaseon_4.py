# -*- coding: utf-8 -*-
"""
Created on Thu May 21 23:05:30 2020

@author: user
"""
from shapely.geometry import Point, Polygon

# Create Point objects
p1 = Point(24.952242, 60.1696017)
p2 = Point(24.976567, 60.1612500)

# Create a Polygon
coords = [(24.950899, 60.169158), (24.953492, 60.169158), (24.953510, 60.170104), (24.950958, 60.169990)]
poly = Polygon(coords)
print(p1)
print(poly)

p1.within(poly)
p2.within(poly)

print(poly.centroid)

poly.contains(p1)
poly.contains(p2)


from shapely.geometry import LineString, MultiLineString
line_a = LineString([(0, 0), (1, 1)])
line_b = LineString([(1, 1), (0, 3)])

line_a.intersects(line_b)
line_a.touches(line_b)

line_a.touches(line_a)
line_a.intersects(line_a)

multi_line = MultiLineString([line_a, line_b])
multi_line


import geopandas as gpd
fp = r"D:\py\gis\Data\addresses.shp"
data = gpd.read_file(fp)
data.head()

import matplotlib.pyplot as plt
gpd.io.file.fiona.drvsupport.supported_drivers['KML'] = 'rw'
fp = r"D:\py\gis\Data\PKS_suuralue.kml"
polys = gpd.read_file(fp, driver='KML')
polys

southern = polys[polys['Name']=='Eteläinen']
southern.reset_index(drop=True, inplace=True)
fig, ax = plt.subplots()
polys.plot(ax=ax, facecolor='gray');
southern.plot(ax=ax, facecolor='red');
data.plot(ax=ax, color='blue', markersize=5);
plt.tight_layout();

import shapely.speedups
shapely.speedups.enable()
pip_mask = data.within(southern.loc[0, 'geometry'])
print(pip_mask)
pip_data = data.loc[pip_mask]
pip_data

southern = polys[polys['Name']=='Eteläinen']
southern.reset_index(drop=True, inplace=True)
fig, ax = plt.subplots()
polys.plot(ax=ax, facecolor='gray');
southern.plot(ax=ax, facecolor='red');
pip_data.plot(ax=ax, color='gold', markersize=2);
plt.tight_layout();



