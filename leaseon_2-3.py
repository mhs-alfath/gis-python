# -*- coding: utf-8 -*-
"""
Created on Sat May  9 08:10:59 2020

@author: user
"""


import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
import matplotlib.pyplot as plt
from geopandas.tools import geocode


fp = r"D:\py\gis\Data\addresses.txt"
data = pd.read_csv(fp, sep=';')
data.head()

geo =geocode(data['addr'], provider='nominatim')
geo.head(2)
join= geo.join(data)
join.head()
join.plot()
plt.tight_layout();

import osmnx as ox
import matplotlib.pyplot as plt
#place_name = "Kamppi, Helsinki, Finland"
place_name = "kemayoran, jakarta, indonesia"
graph = ox.graph_from_place(place_name)
fig, ax = ox.plot_graph(graph)
plt.tight_layout()

area = ox.gdf_from_place(place_name)
buildings = ox.footprints_from_place(place_name)
nodes, edges = ox.graph_to_gdfs(graph)
nodes.head()

fig, ax = plt.subplots()
area.plot(ax=ax, facecolor='black')
edges.plot(ax=ax, linewidth=1, edgecolor='#BC8F8F')
buildings.plot(ax=ax, facecolor='khaki', alpha=0.7)
plt.tight_layout();








